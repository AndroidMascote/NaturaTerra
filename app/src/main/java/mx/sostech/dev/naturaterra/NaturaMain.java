package mx.sostech.dev.naturaterra;

import android.app.Activity;
import android.os.Bundle;

public class NaturaMain extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.natura_main);
    }
}
